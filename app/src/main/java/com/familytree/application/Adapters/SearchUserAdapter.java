package com.familytree.application.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.familytree.application.Activity.AddMemberActivity;
import com.familytree.application.Model.tree.TreeList;
import com.familytree.application.R;
import com.familytree.application.fragment.HomeListFragment;

import java.util.ArrayList;

public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.MyViewHolder> {
    private ArrayList<String> mDataset;
    private AddMemberActivity addMemberActivity;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public TextView tvUser;
        public RelativeLayout parent;
        public MyViewHolder(View v) {
            super(v);


            tvUser = v.findViewById(R.id.search_item);
            parent = v.findViewById(R.id.parent);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SearchUserAdapter(ArrayList<String> myDataset, AddMemberActivity addMemberActivity) {
        mDataset = myDataset;
        this.addMemberActivity = addMemberActivity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SearchUserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String name = mDataset.get(position);

        holder.tvUser.setText(name);
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMemberActivity.onItemClick(position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
