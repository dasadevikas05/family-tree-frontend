package com.familytree.application.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.familytree.application.Model.User;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.SharedPreferencesUtils;
import com.familytree.application.Utils.Util;
import com.familytree.application.Utils.Validation;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView btn_login;
    private TextView tv_forgot_password, tv_register;
    private EditText et_email, et_password;
    private Validation validation;
    private String access_token,
            token_type, refresh_token,expires_in,scope;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

    }

    private void init() {

        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        tv_forgot_password = findViewById(R.id.tv_forgot_password);
        tv_register = findViewById(R.id.tv_register);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        SharedPreferencesUtils.getInstance().initializeSharePreferences(this);

        tv_forgot_password.setOnClickListener(this);
        tv_register.setOnClickListener(this);
        validation = new Validation();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_login:

                verifyLoginData();


                break;


            case R.id.tv_forgot_password:
                Intent forgotIntent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(forgotIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                break;

            case R.id.tv_register:
                Intent signUpIntent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(signUpIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                break;

        }


    }

    private void verifyLoginData() {


        if (et_email.getText().toString().isEmpty() || et_password.getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.empty_fields, Toast.LENGTH_SHORT).show();
            return;

        }else if (!Util.isEmailValid(et_email.getText().toString().trim())){
            Toast.makeText(this, "Please enter valid email id  ", Toast.LENGTH_SHORT).show();
            return;

        }
        makeLoginRequest();
    }


    public void showLockDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Your account has been locked, Please verify your mobile number to activate account.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        Intent dashboardIntent = new Intent(LoginActivity.this, OtpVerificationActivity.class);
                        dashboardIntent.putExtra("username", et_email.getText().toString());
                        startActivity(dashboardIntent);

                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                });
        alertDialog.show();
    }


    public void showBadCredintialDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("You have entered an invalid username or password");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void makeLoginRequest() {

        final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage("Loading...");
        dialog.show();
        String url = Constants.MAIN_URL + Constants.METHOD_LOGIN;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            Log.e(Constants.LOG_TAG,"LoginResponse::"+response);
                            User user  = new Gson().fromJson(response, User.class);
                            if(user != null) {
                                Util.saveUser(response, user);
                                Log.e("TreeUser ", user.getAccessToken());
                                if (!user.getUserDetails().getForgetPassword()) {
                                    Intent dashboardIntent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(dashboardIntent);
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    finish();
                                } else {
                                    Intent dashboardIntent = new Intent(LoginActivity.this, ResetActivity.class);
                                    startActivity(dashboardIntent);
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    finish();
                                }
                            }
                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();

                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            if(data.getString("error_description").equals("TreeUser account is locked")) {
                                showLockDialog();
                            } else if(data.getString("error_description").equals("Bad credentials")) {
                                showBadCredintialDialog();
                            }
                            Log.e("responseBody", ""+responseBody);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        Log.e("Message", ""+error.networkResponse.data);
//                        Log.e("toString", ""+error.toString());
//                        Log.e("toString", ""+error.getLocalizedMessage());
//                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                })


        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", et_email.getText().toString());
                params.put("password", et_password.getText().toString());
                params.put("grant_type", "password");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Basic bW9iaWxlOnBpbg==");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }


}
