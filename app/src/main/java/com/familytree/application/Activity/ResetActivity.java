package com.familytree.application.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.familytree.application.Model.User;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.SharedPreferencesUtils;
import com.familytree.application.Utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetActivity extends AppCompatActivity {

    private EditText et_old_password,et_new_password,et_confirm_password;
    ImageView btn_reset_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);

        init();
    }

    private void init() {

        et_old_password = findViewById(R.id.et_old_password);
        et_new_password = findViewById(R.id.et_new_password);
        et_confirm_password = findViewById(R.id.et_confirm_password);
        SharedPreferencesUtils.getInstance().initializeSharePreferences(this);

        btn_reset_pass = findViewById(R.id.btn_reset_password);

        btn_reset_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validateResetPasswordFields();
            }
        });




    }

    private void validateResetPasswordFields() {

        if (et_old_password.getText().toString().isEmpty()||
        et_confirm_password.getText().toString().isEmpty()||
        et_new_password.getText().toString().isEmpty()){

            Toast.makeText(this, R.string.empty_fields, Toast.LENGTH_SHORT).show();

            return;
        }
        if (!et_new_password.getText().toString().equals(et_confirm_password.getText().toString())){
            Toast.makeText(this, R.string.password_not_match, Toast.LENGTH_SHORT).show();
            return;
        }

        makeResetPasswordReq();

    }


    public void showErrorDialog(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(ResetActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        Util.saveUser(null, null);
                        Intent intent = new Intent(ResetActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
        alertDialog.show();
    }

    private void makeResetPasswordReq() {


        final ProgressDialog dialog = new ProgressDialog(ResetActivity.this);
        dialog.setMessage("Loading...");
        dialog.show();
        String url = Constants.MAIN_URL+Constants.METHOD_RESET_PASSWORD;


        Map<String, String> params = new HashMap();
        params.put("oldPassword", et_old_password.getText().toString());
        params.put("newPassword", et_new_password.getText().toString());

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                //TODO: handle success

                try {
                    String message = response.getString("message");
                    showErrorDialog("Success", message);
//                    Toast.makeText(ResetActivity.this, message, Toast.LENGTH_SHORT).show();

                    Log.e("response",response.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                Toast.makeText(ResetActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
//                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialog.dismiss();
                //TODO: handle failure
                Toast.makeText(ResetActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders()throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization",
                        "bearer  "+ Util.getUser().getAccessToken());
                return params;
            }


        };

        Volley.newRequestQueue(this).add(jsonRequest);








    }
}
