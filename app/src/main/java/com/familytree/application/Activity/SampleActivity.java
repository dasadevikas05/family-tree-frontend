package com.familytree.application.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;

import com.familytree.application.R;

public class SampleActivity extends AppCompatActivity {


    RelativeLayout layout_2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tree_layout);
        float density = getResources().getDisplayMetrics().density;

        layout_2 = findViewById(R.id.layout_2);
//
//        float layout2Rotation = (float) ((density* layout_2.getRotation())/3.0);
//        layout_2.setRotation(layout2Rotation);
//        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) layout_2.getLayoutParams();
//        params.setMarginStart((int) (params.getMarginStart()*density/90));
//
//        Log.e("Start margin ", ""+params.getMarginStart());


//        layout_2.setRotation(90);


        Log.e("Density ",""+getResources().getDisplayMetrics().density);
        Log.e("Density Name ",""+getDensityName());

    }


    private String getDensityName() {
        float density = getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "ldpi";
    }





}
