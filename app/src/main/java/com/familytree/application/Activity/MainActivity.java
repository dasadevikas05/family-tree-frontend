package com.familytree.application.Activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.fragment.HomeFragment;
import com.familytree.application.fragment.HomeListFragment;
import com.familytree.application.fragment.MoreFragment;
import com.familytree.application.fragment.ProfileFragment;
import com.familytree.application.fragment.SettingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_item1:
                                selectedFragment = HomeListFragment.newInstance();
                                break;
                            case R.id.action_item2:
                                selectedFragment = ProfileFragment.newInstance();
                                break;
                            case R.id.action_item3:
                                selectedFragment = SettingFragment.newInstance();
                                break;
                            case R.id.action_item4:
                                selectedFragment = MoreFragment.newInstance();
                                break;

                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                       transaction.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_in_right);
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });
        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, HomeListFragment.newInstance());
        transaction.commit();


    }



    public void addHomeFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_in_right);
        transaction.addToBackStack("Home");
        transaction.add(R.id.frame_layout,  HomeFragment.newInstance());
        transaction.commit();
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            int stackCount = getSupportFragmentManager().getBackStackEntryCount();

            getSupportFragmentManager().popBackStackImmediate();
            List<Fragment> fragments = getSupportFragmentManager().getFragments();

            if(fragments.size()>0 && fragments.get(fragments.size() - 1) instanceof HomeListFragment) {
               HomeListFragment homeListFragment = (HomeListFragment) fragments.get(fragments.size() - 1);
               homeListFragment.refreshFragment();
            }

        }
        else super.onBackPressed();
    }
}
