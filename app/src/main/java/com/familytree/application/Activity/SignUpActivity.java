package com.familytree.application.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.SharedPreferencesUtils;
import com.familytree.application.Utils.Util;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.webservice.VolleyWebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, VolleyCallBack {

    private ImageView btn_sign_up;
    private TextView tv_sign_up_login;
    private EditText et_full_name,et_email_register,et_mobile,et_password_regiser;
    private CheckBox checkbox_terms;
    private boolean isChecked =false;
    private VolleyWebServices webservice = null;
    private RequestQueue mRequestQueue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        init();
    }

    private void init() {

        btn_sign_up = findViewById(R.id.btn_sign_up);
        tv_sign_up_login = findViewById(R.id.tv_sign_up_login);
        mRequestQueue = Volley.newRequestQueue(SignUpActivity.this);
        et_full_name = findViewById(R.id.et_full_name);
        et_email_register = findViewById(R.id.et_email_register);
        et_mobile = findViewById(R.id.et_mobile);
        et_password_regiser = findViewById(R.id.et_password_regiser);
        checkbox_terms = findViewById(R.id.checkbox_terms);

        webservice = new VolleyWebServices(SignUpActivity.this);
        webservice.setListener(this);

        btn_sign_up.setOnClickListener(this);
        tv_sign_up_login.setOnClickListener(this);

        checkbox_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                // Check which checkbox was clicked
                if (checked){
                    // Do your coding
                    isChecked = true;
                }
                else{
                    isChecked=false;
                    // Do your coding
                }
            }
        });

       
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_sign_up:

                checkRegisterValidation();

                break;

            case R.id.tv_sign_up_login:
                //TODO: add animation for backslide
                finish();
//                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                break;

        }


    }

    private void checkRegisterValidation() {
       
        if (et_email_register.getText().toString().isEmpty()||
                et_full_name.getText().toString().isEmpty()||
                et_mobile.getText().toString().isEmpty()||et_password_regiser.getText().toString().isEmpty()){
            Toast.makeText(this, R.string.empty_fields, Toast.LENGTH_SHORT).show();
            return;
        }else if (!Util.isEmailValid(et_email_register.getText().toString().trim())) {
            et_email_register.requestFocus();
            et_email_register.setError(getString(R.string.email_validation));
            return;
        }
        else if (isChecked==false){
            Toast.makeText(this, "Please agree terms and conditions", Toast.LENGTH_SHORT).show();
            return;
        }
            makeRegistrationRequest();

        


    }

    public void showAlert(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(SignUpActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    private void makeRegistrationRequest() {

        String url = Constants.MAIN_URL + Constants.METHOD_REGISTER_USER;
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("name", et_full_name.getText().toString());
            jsonObject.put("email", et_email_register.getText().toString());
            jsonObject.put("password", et_password_regiser.getText().toString());
            jsonObject.put("mobile", et_mobile.getText().toString());
            jsonObject.put("termescondition", isChecked);
            Log.e("params",jsonObject.toString());

        }catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(Constants.LOG_TAG, "Parametres: " + jsonObject.toString());
        webservice.myJsonPostRequest(true,
                url,
                Constants.METHOD_REGISTER_USER,
                jsonObject);

    }

    @Override
    public void getResponse(JSONObject jsonObject, String tag) {


        if (tag.equalsIgnoreCase(Constants.METHOD_REGISTER_USER)) {
            try {

//                Toast.makeText(SignUpActivity.this, R.string.registered, Toast.LENGTH_LONG).show();

//                String id = jsonObject.getString("id");
                String username = jsonObject.getString("username");
//                String email = jsonObject.getString("email");
//                String address = jsonObject.getString("address");
//                String workdetail = jsonObject.getString("workdetail");
//                String nickname = jsonObject.getString("nickname");
//                String name = jsonObject.getString("name");
//                String mobile = jsonObject.getString("mobile");
//                String forgetPassword = jsonObject.getString("forgetPassword");
                String otp = jsonObject.getString("otp");

//                SharedPreferencesUtils.getInstance().writeStringValue(
//                        SharedPreferencesUtils.MOBILE, mobile);
//
//                SharedPreferencesUtils.getInstance().writeStringValue(
//                        SharedPreferencesUtils.NAME, name);
//                SharedPreferencesUtils.getInstance().writeStringValue(
//                        SharedPreferencesUtils.EMAIL, email);


                Intent dashboardIntent = new Intent(SignUpActivity.this, OtpVerificationActivity.class);
                dashboardIntent.putExtra("otp",otp);
                dashboardIntent.putExtra("username",username);
                startActivity(dashboardIntent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);





            } catch (JSONException e) {
                e.printStackTrace();


            }
        }

    }

    @Override
    public void getResponse(JSONArray jsonArray, String tag) {


    }

    @Override
    public void getResponse(String s, String tag) {

    }

    @Override
    public void getError(String error, String tag) {


            try {
                JSONObject data = new JSONObject(error);
                showAlert("Error", data.getString("message"));
                Log.e("responseBody", ""+error);
            }  catch (JSONException e) {
                e.printStackTrace();
            }



    }

    @Override
    public void displayFailedResponse(String failedMessage, String tag) {

    }
}
