package com.familytree.application.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.Util;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.webservice.VolleyWebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener, VolleyCallBack {
    private ImageView btn_forgot_password;
    private EditText et_email_forget_password;
    private VolleyWebServices webservice = null;
    private RequestQueue mRequestQueue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        init();
    }

    private void init() {
        btn_forgot_password = findViewById(R.id.btn_forgot_password);
        et_email_forget_password = findViewById(R.id.et_email_forget_password);
        btn_forgot_password.setOnClickListener(this);
        mRequestQueue = Volley.newRequestQueue(ForgotPasswordActivity.this);
        webservice = new VolleyWebServices(ForgotPasswordActivity.this);
        webservice.setListener(this);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextual_menu, menu);

    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {



        switch (item.getItemId()) {
            case R.id.item_open_activity:

                Intent dashIntent = new Intent(ForgotPasswordActivity.this,LoginActivity.class);
                startActivity(dashIntent);
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn_forgot_password:

                validateEmail();

                break;

        }

    }

    private void validateEmail() {

        if (et_email_forget_password.getText().toString().isEmpty()){

            Toast.makeText(this, R.string.empty_fields, Toast.LENGTH_SHORT).show();
            return;
        }
        if (!Util.isEmailValid(et_email_forget_password.getText().toString().trim())){
            Toast.makeText(this, R.string.email_validation, Toast.LENGTH_SHORT).show();
        return;

        }

        sendPasswordOnMail();




    }

    private void sendPasswordOnMail() {



        String url = Constants.MAIN_URL + Constants.METHOD_FORGET_PASSWORD;
        Log.e("forgetpass",url);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("email", et_email_forget_password.getText().toString());

        }catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(Constants.LOG_TAG, "Parametres: " + jsonObject.toString());
        webservice.myJsonPostRequest(true,
                url,
                Constants.METHOD_FORGET_PASSWORD,
                jsonObject);

    }

    @Override
    public void getResponse(JSONObject jsonObject, String tag) {

        try {
            String message = jsonObject.getString("message");

            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void getResponse(JSONArray jsonArray, String tag) {

    }

    @Override
    public void getResponse(String s, String tag) {

    }

    @Override
    public void getError(String error, String tag) {

    }

    @Override
    public void displayFailedResponse(String failedMessage, String tag) {

    }
}
