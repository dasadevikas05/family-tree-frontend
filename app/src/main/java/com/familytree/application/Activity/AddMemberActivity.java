package com.familytree.application.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.JsonReader;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.familytree.application.Adapters.SearchUserAdapter;
import com.familytree.application.Model.tree.TreeList;
import com.familytree.application.Model.tree.TreeUser;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.Util;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.webservice.VolleyWebServices;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class AddMemberActivity extends AppCompatActivity implements View.OnClickListener, VolleyCallBack {


    private LinearLayout permitMain;
    private LinearLayout privateMain;
    private LinearLayout permitRadio;
    private LinearLayout privateRadio;
    private EditText etUserName;
    private RecyclerView recycler_view;
    private SearchUserAdapter searchUserAdapter;
    private ArrayList<String> userList = new ArrayList<>();
    private VolleyWebServices webservice = null;
    private TextView tvHeader;
    private Button btDone;
    private boolean isItemSelected = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);


        tvHeader = findViewById(R.id.header);
        permitMain = findViewById(R.id.permit);
        privateMain = findViewById(R.id.private_text);
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view.setVisibility(View.GONE);

        permitRadio = findViewById(R.id.pemit_radio_selected);
        privateRadio = findViewById(R.id.private_radio_selected);

        btDone = findViewById(R.id.btn_add_member);

        etUserName = findViewById(R.id.et_email);
        webservice = new VolleyWebServices(AddMemberActivity.this);
        webservice.setListener(this);


        searchUserAdapter = new SearchUserAdapter(userList, this);
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(searchUserAdapter);


        btDone.setOnClickListener(this);


        tvHeader.setText(Util.getCurrentTree().getTreeDAO().getName());

        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!isItemSelected) {
                    if (charSequence.length() > 3) {
                        searchMember(charSequence.toString());
                    } else {
                        userList.clear();
                        searchUserAdapter.notifyDataSetChanged();
                        recycler_view.setVisibility(View.GONE);
                    }
                } else {
                    isItemSelected = false;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        etUserName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if (!b) {
                    recycler_view.setVisibility(View.GONE);
                }

            }
        });

        permitMain.setOnClickListener(this);
        privateMain.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.permit:
                permitRadio.setVisibility(View.VISIBLE);
                privateRadio.setVisibility(View.GONE);
                break;
            case R.id.private_text:
                permitRadio.setVisibility(View.GONE);
                privateRadio.setVisibility(View.VISIBLE);
                break;

            case R.id.btn_add_member:


                String usrename = etUserName.getText().toString().trim();


                if(usrename.isEmpty()) {
                    showDialog("Error", "Please select username.");
                    return;
                }

                if(permitRadio.getVisibility() == View.GONE && privateRadio.getVisibility() == View.GONE) {
                    showDialog("Error", "Please select member permission.");
                    return;
                }



                if (usrename.length() > 1) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("username", usrename);
                        jsonObject.put("treeId", Util.getCurrentTree().getTreeDAO().getId());


                        if(permitRadio.getVisibility() == View.VISIBLE) {
                            jsonObject.put("role", "ADMIN");
                        } else {
                            jsonObject.put("role", "MEMBER");
                        }

                        String url = Constants.MAIN_URL + Constants.METHOD_ADD_MEMBER;
                        webservice.post(jsonObject, url, "ADD_MEMBER");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    showDialog("Error", "Username should be atleast 2 charachter.");
                }
                break;
        }
    }


    private void searchMember(String name) {
        String url = Constants.MAIN_URL + Constants.METHOD_SEARCH_MEMBER + "?name=" + name;
        webservice.get(url, "SEARCH_MEMBER", false);

    }


    public void showDialog(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(AddMemberActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    public void showSuccessDialog(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(AddMemberActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ADD MORE MEMBER",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        etUserName.setText("");
                        permitRadio.setVisibility(View.GONE);
                        privateRadio.setVisibility(View.GONE);
                     }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "NO, THANKS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        finish();
                    }
                });
        alertDialog.show();
    }



    public void onItemClick(int position) {

     isItemSelected = true;
     recycler_view.setVisibility(View.GONE);
     etUserName.setText(userList.get(position));
        etUserName.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etUserName.getWindowToken(), 0);
    }

    @Override
    public void getResponse(JSONObject jsonObject, String tag) {
        if (tag.equals("ADD_MEMBER")) {

            TreeUser treeUser = new Gson().fromJson(jsonObject.toString(), TreeUser.class);


            if(treeUser != null) {
                Util.getCurrentTree().getUsers().add(treeUser);
                showSuccessDialog("Success", "Member added successfully.");
            }

        }
    }

    @Override
    public void getResponse(JSONArray jsonArray, String tag) {

    }

    @Override
    public void getResponse(String s, String tag) {

        Log.e("Search Member::", s);

        if (tag.equals("SEARCH_MEMBER")) {
            try {
                JSONArray jsonArray = new JSONArray(s);
                userList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    userList.add(jsonArray.getString(i));
                }
                if (userList.size() > 0) {
                    recycler_view.setVisibility(View.VISIBLE);
                } else {
                    recycler_view.setVisibility(View.GONE);
                }
                searchUserAdapter.notifyDataSetChanged();
                Log.e("Search Member size::", "" + userList.size());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void getError(String error, String tag) {


        if(tag.equals("ADD_MEMBER")) {
            try {
                JSONObject jsonObject = new JSONObject(error);
                if (jsonObject != null) {
                    showDialog("Error", jsonObject.getString("message"));
                } else {
                    showDialog("Error", "Something went wrong, Please try again later.");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                showDialog("Error", "Something went wrong, Please try again later.");
            }
            Log.e("Error", error);
        }
    }

    @Override
    public void displayFailedResponse(String failedMessage, String tag) {

    }

    public boolean getRecycleViewVisiblity(MotionEvent event) {


        if (recycler_view.getVisibility() == View.VISIBLE) {
            Rect recycleRect = new Rect();
            recycler_view.getGlobalVisibleRect(recycleRect);
            if (!recycleRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {

                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY()) && getRecycleViewVisiblity(event)) {
                    v.clearFocus();
                    recycler_view.setVisibility(View.GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}


