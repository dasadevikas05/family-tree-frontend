package com.familytree.application.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.familytree.application.Model.User;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.Util;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.webservice.VolleyWebServices;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class CreateTreeActivity extends AppCompatActivity implements View.OnClickListener, VolleyCallBack {

    private EditText et_tree_name;
    private Button btn_create_tree;
    private ImageView treeImage;
    private TextView tvExist;

    private VolleyWebServices webservice = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tree);
        init();
    }

    private void init() {

        et_tree_name = findViewById(R.id.et_tree_name);
        btn_create_tree = findViewById(R.id.btn_create_tree);
        treeImage = findViewById(R.id.treeImage);
        tvExist = findViewById(R.id.nameexist);
        btn_create_tree.setOnClickListener(this);
        treeImage.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        webservice = new VolleyWebServices(CreateTreeActivity.this);
        webservice.setListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn_create_tree:

                if(!et_tree_name.getText().toString().trim().isEmpty()) {
                    tvExist.setVisibility(View.GONE);
                    webservice.createTree(et_tree_name.getText().toString().trim());
                } else {
                    showDialog("Error", "Please enter your tree name");
                }
                break;
        }

    }


    public void showDialog(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(CreateTreeActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void getResponse(JSONObject jsonObject, String tag) {

        try {
            String name= jsonObject.getString("name");

            Toast.makeText(this, name+" added as new tree Successfully", Toast.LENGTH_SHORT).show();
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    @Override
    public void getResponse(JSONArray jsonArray, String tag) {


    }

    @Override
    public void getResponse(String s, String tag) {

    }

    @Override
    public void getError(String error, String tag) {
        try {
            JSONObject jsonObject = new JSONObject(error);
            if(jsonObject.getInt("status") == 208 && jsonObject.getString("message").contains("Already exist")) {
                tvExist.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void displayFailedResponse(String failedMessage, String tag) {

    }
}
