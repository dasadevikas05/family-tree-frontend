package com.familytree.application.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.MediaController;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.familytree.application.Model.User;
import com.familytree.application.R;
import com.familytree.application.Utils.SharedPreferencesUtils;
import com.familytree.application.Utils.Util;
import com.familytree.application.network.Internet;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class SplashScreenActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 4000;
    private CoordinatorLayout snackbarCoordinatorLayout;
    private TextView tv_splash;
    private GifImageView gifImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        gifImageView = findViewById(R.id.gif);
        final MediaController mc = new MediaController(this);
        mc.setMediaPlayer((GifDrawable) gifImageView.getDrawable());
        gifImageView.setFreezesAnimation(true);
        SharedPreferencesUtils.getInstance().initializeSharePreferences(this);
        final String user =   SharedPreferencesUtils.getInstance().readStringValue(SharedPreferencesUtils.USER, null);
        Log.e("TreeUser", ""+user);

        ((GifDrawable) gifImageView.getDrawable()).addAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationCompleted(int loopNumber) {
                Log.e("Animation Completed ", ""+loopNumber);
                ((GifDrawable) gifImageView.getDrawable()).stop();
                if(user != null) {
                    User userObject  = new Gson().fromJson(user, User.class);
                    if(userObject != null) {
                        Util.saveUser(user, userObject);
                        if(!userObject.getUserDetails().getForgetPassword()) {
                            Intent mainIntent = new Intent(SplashScreenActivity.this, MainActivity.class);
                            startActivity(mainIntent);
                            SplashScreenActivity.this.finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();
                        } else {
                            Intent mainIntent = new Intent(SplashScreenActivity.this, ResetActivity.class);
                            startActivity(mainIntent);
                            SplashScreenActivity.this.finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();
                        }
                    } else {
                        Intent mainIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(mainIntent);
                        SplashScreenActivity.this.finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                } else {
                    Intent mainIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    startActivity(mainIntent);
                    SplashScreenActivity.this.finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                }
            }
        });


    }



}
