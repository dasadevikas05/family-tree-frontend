package com.familytree.application.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.familytree.application.Model.User;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.SharedPreferencesUtils;
import com.familytree.application.Utils.Util;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class UpdateProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText
            et_address_update, et_nick_update, et_work_update, et_password_update;
    private Button btn_change;
    private TextView et_mobile_update,et_email_update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        init();
    }

    private void init() {

        et_mobile_update = findViewById(R.id.et_mobile_update);
        et_mobile_update.setText(Util.getUser().getUserDetails().getMobile());

        et_email_update = findViewById(R.id.et_email_update);
        et_email_update.setText(Util.getUser().getUserDetails().getEmail());

        et_address_update = findViewById(R.id.et_address_update);
        et_password_update = findViewById(R.id.et_change_password);
        et_password_update.setKeyListener(null);

        if(Util.getUser().getUserDetails().getAddress() != null) {
            et_address_update.setText(Util.getUser().getUserDetails().getAddress());
        }
        et_nick_update = findViewById(R.id.et_nick_update);
        if(Util.getUser().getUserDetails().getNickname() != null) {
            et_nick_update.setText(Util.getUser().getUserDetails().getNickname());
        }

        et_work_update = findViewById(R.id.et_work_update);
        if(Util.getUser().getUserDetails().getWorkdetail() != null) {
            et_work_update.setText(Util.getUser().getUserDetails().getWorkdetail());
        }

        btn_change = findViewById(R.id.btn_change);

        btn_change.setOnClickListener(this);

        et_password_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dashboardIntent = new Intent(UpdateProfileActivity.this, ResetActivity.class);
                startActivity(dashboardIntent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        SharedPreferencesUtils.getInstance().initializeSharePreferences(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_change:
                validateAllFields();

                break;

        }


    }

    private void validateAllFields() {

        if (et_address_update.getText().toString().trim().isEmpty() &&
                et_nick_update.getText().toString().trim().isEmpty() &&
                et_work_update.getText().toString().trim().isEmpty()) {

            Toast.makeText(this, "You dont have anything to update", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!Util.isEmailValid(et_email_update.getText().toString().trim())) {
            Toast.makeText(this, R.string.email_validation, Toast.LENGTH_SHORT).show();
            return;
        }

        updateProfileDetailsReq();


    }


    public void showAlert(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(UpdateProfileActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void updateProfileDetailsReq() {


    final ProgressDialog dialog = new ProgressDialog(UpdateProfileActivity.this);
        dialog.setMessage("Loading...");
        dialog.show();

        String url = Constants.MAIN_URL + Constants.METHOD_UPDATE_USER_DETAILS;

        Map<String, String> params = new HashMap();

        if(!et_work_update.getText().toString().trim().isEmpty()) {
            params.put("workdetails", et_work_update.getText().toString());
        }

        if(!et_address_update.getText().toString().trim().isEmpty()) {
            params.put("address", et_address_update.getText().toString());
        }

        if(!et_nick_update.getText().toString().trim().isEmpty()) {
            params.put("nickname", et_nick_update.getText().toString());
        }
        Log.e("post param",params.toString());
        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                //TODO: handle success

                User user = Util.getUser();

                if(!et_work_update.getText().toString().trim().isEmpty()) {
                    user.getUserDetails().setWorkdetail(et_work_update.getText().toString().trim());
                }

                if(!et_address_update.getText().toString().trim().isEmpty()) {
                    user.getUserDetails().setAddress(et_address_update.getText().toString().trim());
                }

                if(!et_nick_update.getText().toString().trim().isEmpty()) {
                    user.getUserDetails().setNickname(et_nick_update.getText().toString().trim());
                }

                String userString = new Gson().toJson(user);
                Util.saveUser(userString, user);
                try {
                    String message = response.getString("message");

                    showAlert("Success", message);
//                    Toast.makeText(UpdateProfileActivity.this, message, Toast.LENGTH_SHORT).show();

                    Log.e("response",response.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if(error.networkResponse.statusCode == 208) {
                    try {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject data = new JSONObject(responseBody);
                        showAlert("Error", data.getString("message"));
                        Log.e("responseBody", ""+responseBody);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                error.printStackTrace();
                dialog.dismiss();
                //TODO: handle failure
                Toast.makeText(UpdateProfileActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders()throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
            params.put("Content-Type", "application/json");
                params.put("Authorization",
                        "bearer  "+Util.getUser().getAccessToken());
                return params;
            }


        };

        Volley.newRequestQueue(this).add(jsonRequest);

    }
}
