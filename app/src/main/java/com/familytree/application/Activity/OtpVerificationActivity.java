package com.familytree.application.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.SharedPreferencesUtils;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.webservice.VolleyWebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OtpVerificationActivity extends AppCompatActivity implements View.OnClickListener, VolleyCallBack {

    private ImageView btn_verify_otp;
    private Character firstNumber, secondNumber, thirdNumber, fourthNumber;
    private EditText et_first,et_second,et_third,et_fourth;
    private TextView tv_count_down,tv_resend;
    private  String otp,usename;
    private VolleyWebServices webservice = null;
    private RequestQueue mRequestQueue = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        init();
        startTimer();

    }

    private void startTimer() {
        new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_count_down.setText("Resend OTP After: " + millisUntilFinished / 1000 + " sec.");
                tv_resend.setEnabled(false);
                // tv_resend.setBackgroundResource(R.drawable.btn_gray);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                tv_count_down.setText("Resend OTP!!");
                tv_resend.setEnabled(true);
                //  tv_resend.setBackgroundResource(R.drawable.btn_blue);
            }

        }.start();
    }

    private void init() {

        tv_count_down = findViewById(R.id.tv_count_down);
        tv_resend = findViewById(R.id.tv_resend);
        et_first = findViewById(R.id.et_first);
        et_second = findViewById(R.id.et_second);
        et_third = findViewById(R.id.et_third);
        et_fourth = findViewById(R.id.et_fourth);
        tv_resend.setOnClickListener(this);

        try {
            otp = getIntent().getExtras().getString("otp", null);
        } catch (Exception e) {
            e.printStackTrace();
        }


        usename =getIntent().getExtras().getString("username");

        if(usename == null) {
            finish();
        }

//        firstNumber = otp.charAt(0);
//        secondNumber = otp.charAt(1);
//        thirdNumber = otp.charAt(2);
//        fourthNumber = otp.charAt(3);

        firstNumber = '0';
        secondNumber = '0';
        thirdNumber = '0';
        fourthNumber = '0';

        et_first.setText("0");
        et_second.setText("0");
        et_third.setText("0");
        et_fourth.setText("0");

        webservice = new VolleyWebServices(OtpVerificationActivity.this);
        webservice.setListener(this);
        btn_verify_otp = findViewById(R.id.btn_verify_otp);
        btn_verify_otp.setOnClickListener(this);


        et_first.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if(b){
                    et_first.setSelection(et_first.getText().length());
                }
            }
        });

        et_second.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if(b){
                    et_second.setSelection(et_second.getText().length());
                }
            }
        });
        et_third.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if(b){
                    et_third.setSelection(et_third.getText().length());
                }
            }
        });
        et_fourth.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if(b){
                    et_fourth.setSelection(et_fourth.getText().length());
                }
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.tv_resend:
                startTimer();
                getResendVerificationCode();

                break;


            case R.id.btn_verify_otp:

                verifyOtp();
                /*
                 */
                break;



        }
    }



    public void showErrorDialog(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(OtpVerificationActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void verifyOtp() {


        String first = et_first.getText().toString();
        String second = et_second.getText().toString();
        String third = et_third.getText().toString();
        String forth = et_fourth.getText().toString();

        if(first.trim().isEmpty() || second.trim().isEmpty() || third.trim().isEmpty() || forth.trim().isEmpty()) {
            showErrorDialog("Error", "Please enter all fields");
            return;
        }

        String otpObj = first+second+third+forth;

        String url = Constants.MAIN_URL + Constants.METHOD_VERIFY_OTP;
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("email",
                usename);
            jsonObject.put("otp", otpObj);

            Log.e("params",jsonObject.toString());

        }catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(Constants.LOG_TAG, "Parametres: " + jsonObject.toString());
        webservice.myJsonPostRequest(true,
                url,
                Constants.METHOD_VERIFY_OTP,
                jsonObject);



    }

    private void getResendVerificationCode() {

    }

    @Override
    public void getResponse(JSONObject jsonObject, String tag) {
        if (tag.equalsIgnoreCase(Constants.METHOD_VERIFY_OTP)) {
            try {

                String message = jsonObject.getString("message");
                Toast.makeText(OtpVerificationActivity.this, message, Toast.LENGTH_SHORT).show();
                Intent verifyOtpIntent = new Intent(OtpVerificationActivity.this, LoginActivity.class);
                startActivity(verifyOtpIntent);
                finish();
                Toast.makeText(this, "Otp Successfully verified", Toast.LENGTH_SHORT).show();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }catch (JSONException e) {
                e.printStackTrace();
                showErrorDialog("Error", "Oops, Something went wrong try again later.");
            }
        }
    }

    @Override
    public void getResponse(JSONArray jsonArray, String tag) {



    }

    @Override
    public void getResponse(String s, String tag) {

    }

    @Override
    public void getError(String error, String tag) {
        try {
            JSONObject jsonObject = new JSONObject(error);
            showErrorDialog("Error", jsonObject.getString("message"));

        } catch (JSONException e) {
            e.printStackTrace();
            showErrorDialog("Error", "Oops, Something went wrong try again later.");
        }
    }

    @Override
    public void displayFailedResponse(String failedMessage, String tag) {

    }
}
