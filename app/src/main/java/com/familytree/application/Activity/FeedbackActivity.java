package com.familytree.application.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.familytree.application.R;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.webservice.VolleyWebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FeedbackActivity extends AppCompatActivity implements VolleyCallBack {

    private Button btnFeedack;
    private EditText etFeedback;
    private ImageView ivBack;
    private VolleyWebServices webservice = null;
    private RequestQueue mRequestQueue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        btnFeedack = findViewById(R.id.btn_feedback);
        etFeedback = findViewById(R.id.et_feedback);
        ivBack = findViewById(R.id.back);

        mRequestQueue = Volley.newRequestQueue(FeedbackActivity.this);
        webservice = new VolleyWebServices(FeedbackActivity.this);
        webservice.setListener(this);


        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnFeedack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!etFeedback.getText().toString().trim().isEmpty()) {
                    webservice.sendFeedback(etFeedback.getText().toString().trim());
                } else {
                    showDialog("Error", "Please enter your feedback");
                }
            }
        });

    }


    public void showDialog(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(FeedbackActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void showDialogToFinish(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(FeedbackActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        finish();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void getResponse(JSONObject jsonObject, String tag) {
        try {
            showDialogToFinish("Success", jsonObject.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getResponse(JSONArray jsonArray, String tag) {

    }

    @Override
    public void getResponse(String s, String tag) {

    }

    @Override
    public void getError(String error, String tag) {

    }

    @Override
    public void displayFailedResponse(String failedMessage, String tag) {

    }
}
