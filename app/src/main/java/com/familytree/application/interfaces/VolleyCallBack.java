package com.familytree.application.interfaces;

import org.json.JSONArray;
import org.json.JSONObject;


public interface VolleyCallBack {
    public void getResponse(JSONObject jsonObject, String tag);
    public void getResponse(JSONArray jsonArray, String tag);
    public void getResponse(String s, String tag);

    public void getError(String error, String tag);

    public void displayFailedResponse(String failedMessage, String tag);
}