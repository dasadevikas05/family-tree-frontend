package com.familytree.application.interfaces;

public interface VolleyStringCallBack {

    public void getResponseString(String dataString, String tag);

    public void getErrorString(String error);

}