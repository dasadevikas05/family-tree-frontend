
package com.familytree.application.Model.tree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TreeDAO {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;
    @SerializedName("user")
    @Expose
    private TreeUser user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public TreeUser getUser() {
        return user;
    }

    public void setUser(TreeUser user) {
        this.user = user;
    }

}
