
package com.familytree.application.Model.tree;

import java.util.List;

import com.familytree.application.Model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TreeDetails {

    @SerializedName("users")
    @Expose
    private List<TreeUser> users = null;
    @SerializedName("treeDAO")
    @Expose
    private TreeDAO treeDAO;

    public List<TreeUser> getUsers() {
        return users;
    }

    public void setUsers(List<TreeUser> users) {
        this.users = users;
    }

    public TreeDAO getTreeDAO() {
        return treeDAO;
    }

    public void setTreeDAO(TreeDAO treeDAO) {
        this.treeDAO = treeDAO;
    }

}
