package com.familytree.application.network;

import android.content.Context;


public class Internet {
    public static boolean isAvailable(Context mContext) {

        InternetCheck check = new InternetCheck(mContext);

        if (check.haveNetworkConnection()) {
            return true;
        }
        else {
            return false;
        }
    }
}