package com.familytree.application.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.familytree.application.Activity.CreateTreeActivity;
import com.familytree.application.Activity.ForgotPasswordActivity;
import com.familytree.application.Activity.MainActivity;
import com.familytree.application.Adapters.TreeListAdapter;
import com.familytree.application.Model.User;
import com.familytree.application.Model.tree.TreeDetails;
import com.familytree.application.Model.tree.TreeList;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.Util;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.webservice.VolleyWebServices;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeListFragment extends Fragment implements VolleyCallBack {


    public static HomeListFragment newInstance() {
        HomeListFragment fragment = new HomeListFragment();
        return fragment;
    }


    public HomeListFragment() {
        // Required empty public constructor
    }



    private RecyclerView recycler_view;
    private VolleyWebServices webservice;
    private TreeListAdapter adapter;
    private ArrayList<TreeList> treeLists = new ArrayList<>();
    private FloatingActionButton floatingActionButton;
    private ImageView treeImage;
    private LinearLayout linearLayout;

    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isFirstLaunch = false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_list, container, false);
        recycler_view = view.findViewById(R.id.recycler_view);
        treeImage = view.findViewById(R.id.treeImage);
        swipeRefreshLayout = view.findViewById(R.id.pullrelresh);
        treeImage.setColorFilter(getResources().getColor(R.color.light_grey), PorterDuff.Mode.SRC_IN);

        floatingActionButton = view.findViewById(R.id.floatingActionButton);
        linearLayout = view.findViewById(R.id.emptyLayout);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //here
                startActivity(new Intent(getActivity(), CreateTreeActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);


            }
        });
        isFirstLaunch = true;
         adapter = new TreeListAdapter(treeLists, this);

        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_view.setAdapter(adapter);

        webservice = new VolleyWebServices(getActivity());
        webservice.setListener(this);
        webservice.get(Constants.MAIN_URL+Constants.METHOD_GET_TREE_LIST, "getList", true);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webservice.get(Constants.MAIN_URL+Constants.METHOD_GET_TREE_LIST, "getList", false);
//                swipeRefreshLayout.setRefreshing(false);
            }
        });
        return view;
    }

    public void onItemClick(int position) {
        webservice.get(Constants.MAIN_URL+Constants.METHOD_GET_TREE_DETAILS+"?id="+treeLists.get(position).getId(), "getDetails", true);
    }


    @Override
    public void onResume() {
        super.onResume();

        if(!isFirstLaunch){
            webservice.get(Constants.MAIN_URL+Constants.METHOD_GET_TREE_LIST, "getList", false);
        } else {
            isFirstLaunch = false;
        }
    }


    public void refreshFragment() {
        webservice.get(Constants.MAIN_URL+Constants.METHOD_GET_TREE_LIST, "getList", false);
    }

    @Override
    public void getResponse(JSONObject jsonObject, String tag) {
    }

    @Override
    public void getResponse(JSONArray jsonArray, String tag) {

    }

    @Override
    public void getResponse(String s, String tag) {
        if(tag.equals("getList")) {
            try {
                swipeRefreshLayout.setRefreshing(false);
                JSONArray jsonArray = new JSONArray(s);
                treeLists.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    TreeList userObject = new Gson().fromJson(jsonObject.toString(), TreeList.class);
                    treeLists.add(userObject);
                }
                adapter.notifyDataSetChanged();
                if(treeLists.size()> 0) {
                    recycler_view.setVisibility(View.VISIBLE);
                    linearLayout.setVisibility(View.GONE);
                } else {
                    recycler_view.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            TreeDetails treeDetails = new Gson().fromJson(s, TreeDetails.class);
            if(treeDetails != null) {
                Util.setCurrentTree(treeDetails);
                MainActivity activity = (MainActivity) getActivity();
                activity.addHomeFragment();
            }
        }
    }

    @Override
    public void getError(String error, String tag) {
        swipeRefreshLayout.setRefreshing(false);
        if(treeLists.size()> 0) {
            recycler_view.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.GONE);
        } else {
            recycler_view.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void displayFailedResponse(String failedMessage, String tag) {
    }



}
