package com.familytree.application.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.IntentCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.familytree.application.Activity.LoginActivity;
import com.familytree.application.Activity.UpdateProfileActivity;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.SharedPreferencesUtils;
import com.familytree.application.Utils.Util;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.webservice.VolleyWebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;


public class SettingFragment extends Fragment implements View.OnClickListener, VolleyCallBack {

    private TextView logout;
    private VolleyWebServices webservice = null;
    private View settingView;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        settingView = inflater.inflate(R.layout.fragment_setting, container, false);

        init();

        return settingView;
    }

    private void init() {

         logout = settingView.findViewById(R.id.logout);
         mContext = getActivity();
         webservice = new VolleyWebServices(mContext);

         logout.setOnClickListener(this);
         webservice.setListener(this);



    }

    private void makeLogoutRequest() {

        String url = Constants.MAIN_URL + Constants.METHOD_LOGOUT_USER;

        webservice.myJsonPostStringRequest(true,
                url,
                Constants.METHOD_LOGOUT_USER,
                null);

    }


    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.logout:

                showLogoutDialog();

                break;

        }

    }

    @Override
    public void getResponse(JSONObject jsonObject, String tag) {

    }

    @Override
    public void getResponse(JSONArray jsonArray, String tag) {

    }

    @Override
    public void getResponse(String s, String tag) {

        if (tag.equalsIgnoreCase(Constants.METHOD_LOGOUT_USER)) {


            try {
                JSONObject jsonobject = new JSONObject(s);
                String message = jsonobject.getString("message");

                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                Util.saveUser(null, null);
                SharedPreferencesUtils.getInstance().clearAllPref();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }




        }

    }

    @Override
    public void getError(String error, String tag) {

        Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();



    }

    @Override
    public void displayFailedResponse(String failedMessage, String tag) {

    }

    private void showLogoutDialog() {
        LayoutInflater li = LayoutInflater.from(mContext);
        @SuppressLint("InflateParams") View promptsView;
        promptsView = li.inflate(R.layout.logout_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        // Setting Dialog Title
        builder.setView(promptsView);
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();
        dialog.show();
        TextView dialogButtonNo = promptsView.findViewById(R.id.dBtnNo);
        TextView dialogButtonYes = promptsView.findViewById(R.id.dBtnYes);

        dialogButtonYes.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View view) {
                                                   dialog.dismiss();
                                                    makeLogoutRequest();

                                               }
                                           }
        );
        dialogButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}
