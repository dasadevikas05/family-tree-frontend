package com.familytree.application.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.familytree.application.Activity.AddMemberActivity;
import com.familytree.application.Activity.UpdateProfileActivity;
import com.familytree.application.Model.User;
import com.familytree.application.Model.tree.TreeUser;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.SharedPreferencesUtils;
import com.familytree.application.Utils.Util;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.webservice.VolleyWebServices;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment implements View.OnClickListener, VolleyCallBack {

     private ImageView iv_view_info;
     private View homeView;
     private Context mContext;
     private TextView treeName;
//     private WebView webView;
     private LinearLayout addLeaf;
     private boolean isFromOnCreate = false;
    PopupWindow  mypopupWindow;
    private VolleyWebServices webservice = null;

    private RelativeLayout leap_1,leap_2,leap_3,leap_4,leap_5,leap_6,leap_7, leap_8,leap_9,leap_10,leap_11,leap_12, leap_13,leap_14,leap_15,leap_16,leap_17,leap_18,leap_19,leap_20;
    private RelativeLayout leap_21,leap_22,leap_23,leap_24,leap_25,leap_26,leap_27, leap_28,leap_29,leap_30,leap_31,leap_32, leap_33,leap_34,leap_35,leap_36,leap_37,leap_38,leap_39,leap_40;
    private RelativeLayout leap_41,leap_42,leap_43,leap_44,leap_45,leap_46,leap_47, leap_48,leap_49,leap_50;


    ArrayList<RelativeLayout> leafList = new ArrayList<RelativeLayout>();

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeView = inflater.inflate(R.layout.fragment_home, container, false);

//        webView = homeView.findViewById(R.id.webview);
        addLeaf = homeView.findViewById(R.id.addLeaf);
        treeName = homeView.findViewById(R.id.treeName);

        treeName.setText(Util.getCurrentTree().getTreeDAO().getName());

        webservice = new VolleyWebServices(getActivity());
        webservice.setListener(this);
        isFromOnCreate = true;
        initLeap(homeView);



        addLeaf.setOnClickListener(this);
        mContext = getActivity();
//        initWebView();

        return homeView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(!isFromOnCreate){
           updateList();
        } else {
           isFromOnCreate = false;
        }
    }

    private  void initLeap(View view) {
        leap_1 = view.findViewById(R.id.layout_1);
        leap_2 = view.findViewById(R.id.layout_2);
        leap_3 = view.findViewById(R.id.layout_3);
        leap_4 = view.findViewById(R.id.layout_4);
        leap_5 = view.findViewById(R.id.layout_5);
        leap_6 = view.findViewById(R.id.layout_6);
        leap_7 = view.findViewById(R.id.layout_7);
        leap_8 = view.findViewById(R.id.layout_8);
        leap_9 = view.findViewById(R.id.layout_9);
        leap_10 = view.findViewById(R.id.layout_10);
        leap_11 = view.findViewById(R.id.layout_11);
        leap_12 = view.findViewById(R.id.layout_12);
        leap_13 = view.findViewById(R.id.layout_13);
        leap_14 = view.findViewById(R.id.layout_14);
        leap_15 = view.findViewById(R.id.layout_15);
        leap_16 = view.findViewById(R.id.layout_16);
        leap_17 = view.findViewById(R.id.layout_17);
        leap_18 = view.findViewById(R.id.layout_18);
        leap_19 = view.findViewById(R.id.layout_19);
        leap_20 = view.findViewById(R.id.layout_20);
        leap_21 = view.findViewById(R.id.layout_21);
        leap_22 = view.findViewById(R.id.layout_22);
        leap_23 = view.findViewById(R.id.layout_23);
        leap_24 = view.findViewById(R.id.layout_24);
        leap_25 = view.findViewById(R.id.layout_25);
        leap_26 = view.findViewById(R.id.layout_26);
        leap_27 = view.findViewById(R.id.layout_27);
        leap_28 = view.findViewById(R.id.layout_28);
        leap_29 = view.findViewById(R.id.layout_29);
        leap_30 = view.findViewById(R.id.layout_30);
        leap_31 = view.findViewById(R.id.layout_31);
        leap_32 = view.findViewById(R.id.layout_32);
        leap_33 = view.findViewById(R.id.layout_33);
        leap_34 = view.findViewById(R.id.layout_34);
        leap_35 = view.findViewById(R.id.layout_35);
        leap_36 = view.findViewById(R.id.layout_36);
        leap_37 = view.findViewById(R.id.layout_37);
        leap_38 = view.findViewById(R.id.layout_38);
        leap_39 = view.findViewById(R.id.layout_39);
        leap_40 = view.findViewById(R.id.layout_40);
        leap_41 = view.findViewById(R.id.layout_41);
        leap_42 = view.findViewById(R.id.layout_42);
        leap_43 = view.findViewById(R.id.layout_43);
        leap_44 = view.findViewById(R.id.layout_44);
        leap_45 = view.findViewById(R.id.layout_45);
        leap_46 = view.findViewById(R.id.layout_46);
        leap_47 = view.findViewById(R.id.layout_47);
        leap_48 = view.findViewById(R.id.layout_48);
        leap_49 = view.findViewById(R.id.layout_49);


        leafList.add(leap_1);
        leafList.add(leap_2);
        leafList.add(leap_3);
        leafList.add(leap_4);
        leafList.add(leap_5);
        leafList.add(leap_6);
        leafList.add(leap_7);
        leafList.add(leap_8);
        leafList.add(leap_9);
        leafList.add(leap_10);
        leafList.add(leap_11);
        leafList.add(leap_12);
        leafList.add(leap_13);
        leafList.add(leap_14);
        leafList.add(leap_15);
        leafList.add(leap_16);
        leafList.add(leap_17);
        leafList.add(leap_18);
        leafList.add(leap_19);
        leafList.add(leap_20);
        leafList.add(leap_21);
        leafList.add(leap_22);
        leafList.add(leap_23);
        leafList.add(leap_24);
        leafList.add(leap_25);
        leafList.add(leap_26);
        leafList.add(leap_27);
        leafList.add(leap_28);
        leafList.add(leap_29);
        leafList.add(leap_30);
        leafList.add(leap_31);
        leafList.add(leap_32);
        leafList.add(leap_33);
        leafList.add(leap_34);
        leafList.add(leap_35);
        leafList.add(leap_36);
        leafList.add(leap_37);
        leafList.add(leap_38);
        leafList.add(leap_39);
        leafList.add(leap_40);
        leafList.add(leap_41);
        leafList.add(leap_42);
        leafList.add(leap_43);
        leafList.add(leap_44);
        leafList.add(leap_45);
        leafList.add(leap_46);
        leafList.add(leap_47);
        leafList.add(leap_48);
        leafList.add(leap_49);
        updateList();
    }


    private void showConfirmationDialog(final TreeUser treeUser) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage("Are you sure? you want to remove \""+treeUser.getName()+"\"");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        JSONObject jsonObject = new JSONObject();

                        try {
                            jsonObject.put("treeUserId", treeUser.getTreeUserId());
                            jsonObject.put("treeId", Util.getCurrentTree().getTreeDAO().getId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String url = Constants.MAIN_URL+Constants.METHOD_REMOVE_MEMBER;
                        webservice.post(jsonObject,url,  Constants.METHOD_REMOVE_MEMBER);
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    private void showPopupMenu(View parent, final TreeUser treeUser) {


        LayoutInflater inflater = (LayoutInflater)
               getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup, null);


        TextView tvPopupHeader = view.findViewById(R.id.popup_header);
        tvPopupHeader.setText(treeUser.getName());


        RelativeLayout removeLayout = view.findViewById(R.id.removeLayout);
        RelativeLayout connectLayout = view.findViewById(R.id.connectLayout);


        connectLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mypopupWindow.dismiss();
            }
        });


        removeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mypopupWindow.dismiss();
                if(!treeUser.getRole().equalsIgnoreCase("OWNER")) {
                    showConfirmationDialog(treeUser);
                } else {
                    showDialog("Error", "You can't remove owner");
                }
            }
        });


        mypopupWindow = new PopupWindow(view,500, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mypopupWindow.setElevation((float) 20.0);
        }
        mypopupWindow.showAsDropDown(parent,-10,-130);
    }

    public void showDialog(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void updateList() {
        int listSize = Util.getCurrentTree().getUsers().size()> 48 ? 48 : Util.getCurrentTree().getUsers().size();
        for(int i =0;i<listSize; i++) {
            RelativeLayout leaplayout =  leafList.get(i);
            leaplayout.setVisibility(View.VISIBLE);
            TextView tvName = (TextView) leaplayout.getChildAt(1);
            TreeUser treeUser =  Util.getCurrentTree().getUsers().get(i);
            String name = "";
            if(treeUser.getName().length()> 5) {
                name = treeUser.getName().substring(0,3)+"...";
            } else {
                name = treeUser.getName();
            }
            tvName.setText(name);
            onItemClick(Util.getCurrentTree().getUsers().get(i), leaplayout);
        }

        for(int i =listSize ;i<leafList.size(); i++) {
            if(leafList.get(i).getVisibility() == View.VISIBLE) {
                leafList.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }



    public void onItemClick(final TreeUser treeUser, final RelativeLayout layout) {

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("treeUser", treeUser.getName());
                    Log.e("treeUserID", ""+treeUser.getTreeUserId());
                    showPopupMenu(layout,treeUser);
                }
            });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addLeaf:
                Intent updateIntent = new Intent(getActivity(), AddMemberActivity.class);
                mContext.startActivity(updateIntent);
                break;
        }

    }

    @Override
    public void getResponse(JSONObject jsonObject, String tag) {


        TreeUser treeUser = new Gson().fromJson(jsonObject.toString(), TreeUser.class);
        Log.e("Resposne",""+treeUser.getTreeUserId());

        List<TreeUser> usreList = Util.getCurrentTree().getUsers();
        int removedUser = -1;
        for(int i = 0; i<usreList.size();i++) {

            int userId = usreList.get(i).getTreeUserId();
            int treeId = treeUser.getTreeUserId();

           if( userId == treeId) {
               removedUser = i;
               break;
           }
        }

        if(removedUser != -1) {
            Util.getCurrentTree().getUsers().remove(removedUser);
        }
        updateList();
    }

    @Override
    public void getResponse(JSONArray jsonArray, String tag) {

    }

    @Override
    public void getResponse(String s, String tag) {

    }

    @Override
    public void getError(String error, String tag) {

    }

    @Override
    public void displayFailedResponse(String failedMessage, String tag) {

    }
}
