package com.familytree.application.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.familytree.application.Activity.AboutUsActivity;
import com.familytree.application.Activity.FeedbackActivity;
import com.familytree.application.Activity.LoginActivity;
import com.familytree.application.R;


public class MoreFragment extends Fragment implements View.OnClickListener {

    private LinearLayout feedback,ll_about_us;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);

        feedback = view.findViewById(R.id.feedback);
        ll_about_us = view.findViewById(R.id.ll_about_us);
        ll_about_us.setOnClickListener(this);
        feedback.setOnClickListener(this);

        // Inflate the layout for this fragment
        return  view;
    }

    public static MoreFragment newInstance() {
        MoreFragment fragment = new MoreFragment();
        return fragment;
    }

    @Override
    public void onClick(View view) {

        if(view == feedback) {

            Intent intent = new Intent(getActivity(), FeedbackActivity.class);
            startActivity(intent);

        }else if (view==ll_about_us){
            Intent aboutUsIntent = new Intent(getActivity(), AboutUsActivity.class);
            startActivity(aboutUsIntent);
        }
    }
}
