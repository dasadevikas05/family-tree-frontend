package com.familytree.application.Utils;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import android.util.DisplayMetrics;
import android.util.StateSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


import androidx.annotation.RequiresApi;

import com.familytree.application.Model.User;
import com.familytree.application.Model.tree.TreeDetails;
import com.familytree.application.R;

import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.familytree.application.Utils.AppUtils.haveNetworkConnection;


/**
 * Created by Weiping Huang at 01:12 on 2016/3/19
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 */

public class Util {

    private static final int[] colors = new int[]{
            Color.parseColor("#2781bd"),
            Color.parseColor("#827563"),
            Color.parseColor("#da8547"),
            Color.parseColor("#e16756"),
            Color.parseColor("#c1bd4d"),
            Color.parseColor("#4478c0"),
            Color.parseColor("#679cc0"),
            Color.parseColor("#9eb15a"),
            Color.parseColor("#585752"),

    };
    private static final ArrayList<Integer> usedColor = new ArrayList<>();
    private static Util ourInstance = new Util();


    private static User user;
    private static TreeDetails currentTree;

    private Util() {
    }

    public static String checkString(String myString) {
        String data = myString.trim();
        if (data.equalsIgnoreCase(null) ||
                data.equalsIgnoreCase("") ||
                data.length() == 0 ||
                data.equalsIgnoreCase("null")) {
            data = "N/A";
        } else {
            data = myString.trim();
        }
        return data;
    }

    public static void validateStringAndSetTextView(TextView textView, String string) {
        if (string.equalsIgnoreCase("") || string.equalsIgnoreCase("null") || string.equalsIgnoreCase(null)) {
            textView.setText("N/A");
        } else {
            textView.setText(string);
        }
    }

    public static Activity scanForActivity(Context context) {
        if (context == null) {
            //   Log.w(BoomMenuButton.TAG, "scanForActivity: context is null!");
            return null;
        } else if (context instanceof Activity)
            return (Activity) context;
        else if (context instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) context).getBaseContext());
        // Log.w(BoomMenuButton, "scanForActivity: context is null!");
        return null;
    }

    public static void setVisibility(int visibility, View... views) {
        for (View view : views) if (view != null) view.setVisibility(visibility);
    }

    public static int dp2px(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static int getColor(View view, int id, Resources.Theme theme) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return view.getResources().getColor(id, theme);
        } else {
            //noinspection deprecation
            return view.getResources().getColor(id);
        }
    }

    public static int getColor(TypedArray typedArray, int id, Resources.Theme theme) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return typedArray.getResources().getColor(id, theme);
        } else {
            //noinspection deprecation
            return typedArray.getResources().getColor(id);
        }
    }

    public static boolean validateContactNumber(String stringContact) {
        return  (stringContact.length() < 10);
    }

    public static void setAdapterClaimIntimation(Context mContext, Spinner spinner_policy_no, ArrayList<String> policyList) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mContext, R.layout
                .spinner_item_claimintimation, policyList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_policy_no.setAdapter(dataAdapter);
    }

    public static int getColor(View view, int id) {
        return getColor(view, id, null);
    }

    public static void setAdapter(Context mContext,
                                  Spinner spinnerPol,
                                  ArrayList<String> policyList) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mContext,
                R.layout.spinner_item_claimintimation,
                policyList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPol.setAdapter(dataAdapter);
    }

    public static int getColor(TypedArray typedArray, int id) {
        return getColor(typedArray, id, null);
    }

    public static Drawable getSystemDrawable(Context context, int id) {
        int[] attrs = new int[]{id};
        TypedArray ta = context.obtainStyledAttributes(attrs);
        Drawable drawable = ta.getDrawable(0);
        ta.recycle();
        return drawable;
    }

    public static boolean isEmailValid(String email) {
        String regExpn =
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    public static Drawable getDrawable(View view, int id, Resources.Theme theme) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return view.getResources().getDrawable(id, theme);
        } else {
            //noinspection deprecation
            return view.getResources().getDrawable(id);
        }
    }

    public static void setDrawable(ImageView image, int id, Drawable drawable) {
        if (image == null) return;
        if (id == 0) {
            if (drawable != null) image.setImageDrawable(drawable);
        } else image.setImageResource(id);
    }

    public static void setText(TextView textView, int id, String text) {
        if (textView == null) return;
        if (id == 0) {
            if (text != null && !text.equals(textView.getText())) textView.setText(text);
        } else {
            CharSequence oldText = textView.getContext().getResources().getText(id);
            if (!oldText.equals(textView.getText())) textView.setText(id);
        }
    }

    public static boolean isInternetAvailable(Context mContext) {
        return  (haveNetworkConnection(mContext));
    }

    public static void setTextColor(TextView textView, int id, int color) {
        if (textView == null) return;
        if (id == 0) {
            textView.setTextColor(color);
        } else textView.setTextColor(getColor(textView.getContext(), id));
    }

    public static Drawable getDrawable(View view, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return view.getResources().getDrawable(id, null);
        } else {
            //noinspection deprecation
            return view.getResources().getDrawable(id);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKatOrAbove = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKatOrAbove && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }


            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                switch (type) {
                    case "image":
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        break;
                    case "video":
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        break;
                    case "audio":
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        break;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }



    public static BitmapDrawable getOvalBitmapDrawable(View view, int radius, int color) {
        if (radius <= 0) {
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(
                2 * radius,
                2 * radius,
                Bitmap.Config.ARGB_8888);
        Canvas canvasPressed = new Canvas(bitmap);
        Paint paintPressed = new Paint();
        paintPressed.setAntiAlias(true);
        paintPressed.setColor(color);
        canvasPressed.drawCircle(
                radius,
                radius,
                radius, paintPressed);
        return new BitmapDrawable(view.getResources(), bitmap);
    }



    public static BitmapDrawable getRectangleBitmapDrawable(View view, int width, int height, int cornerRadius, int color) {
        if (width <= 0 || height <= 0) {
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(
                width,
                height,
                Bitmap.Config.ARGB_8888);
        Canvas canvasPressed = new Canvas(bitmap);
        Paint paintPressed = new Paint();
        paintPressed.setAntiAlias(true);
        paintPressed.setColor(color);
        canvasPressed.drawRoundRect(new RectF(0, 0, width, height), cornerRadius, cornerRadius, paintPressed);
        return new BitmapDrawable(view.getResources(), bitmap);
    }

    public static float distance(Point a, Point b) {
        return (float) Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }

    // Bitmap drawable in state-list drawable is able to perform a click-effect.
    public static StateListDrawable getOvalStateListBitmapDrawable(View view,
                                                                   int radius,
                                                                   int normalColor,
                                                                   int highlightedColor,
                                                                   int unableColor) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(
                new int[]{android.R.attr.state_pressed},
                getOvalBitmapDrawable(view, radius, highlightedColor));
        stateListDrawable.addState(
                new int[]{-android.R.attr.state_enabled},
                getOvalBitmapDrawable(view, radius, unableColor));
        stateListDrawable.addState(
                StateSet.WILD_CARD,
                getOvalBitmapDrawable(view, radius, normalColor));
        return stateListDrawable;
    }

    // Gradient drawable in state-list drawable is not able to perform a click-effect.
    public static StateListDrawable getOvalStateListGradientDrawable(View view,
                                                                     int normalColor,
                                                                     int highlightedColor,
                                                                     int unableColor) {
        StateListDrawable stateListDrawable = new StateListDrawable();
     /*   stateListDrawable.addState(
                new int[]{android.R.attr.state_pressed},
                getOvalDrawable(view, highlightedColor));
        stateListDrawable.addState(
                new int[]{-android.R.attr.state_enabled},
                getOvalDrawable(view, unableColor));
        stateListDrawable.addState(
                StateSet.WILD_CARD,
                getOvalDrawable(view, normalColor));*/
        return stateListDrawable;
    }

    // Bitmap drawable in state-list drawable is able to perform a click-effect.
    public static StateListDrawable getRectangleStateListBitmapDrawable(View view,
                                                                        int width,
                                                                        int height,
                                                                        int cornerRadius,
                                                                        int normalColor,
                                                                        int highlightedColor,
                                                                        int unableColor) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(
                new int[]{android.R.attr.state_pressed},
                getRectangleBitmapDrawable(view, width, height, cornerRadius, highlightedColor));
        stateListDrawable.addState(
                new int[]{-android.R.attr.state_enabled},
                getRectangleBitmapDrawable(view, width, height, cornerRadius, unableColor));
        stateListDrawable.addState(
                StateSet.WILD_CARD,
                getRectangleBitmapDrawable(view, width, height, cornerRadius, normalColor));
        return stateListDrawable;
    }

    // Gradient drawable in state-list drawable is not able to perform a click-effect.
    public static StateListDrawable getRectangleStateListGradientDrawable(View view,
                                                                          int cornerRadius,
                                                                          int normalColor,
                                                                          int highlightedColor,
                                                                          int unableColor) {
        StateListDrawable stateListDrawable = new StateListDrawable();
       /* stateListDrawable.addState(
                new int[]{android.R.attr.state_pressed},
                getRectangleDrawable(view, cornerRadius, highlightedColor));
        stateListDrawable.addState(
                new int[]{-android.R.attr.state_enabled},
                getRectangleDrawable(view, cornerRadius, unableColor));
        stateListDrawable.addState(
                StateSet.WILD_CARD,
                getRectangleDrawable(view, cornerRadius, normalColor));*//**/
        return stateListDrawable;
    }

    public static int getInt(
            TypedArray typedArray,
            int id,
            int defaultId) {
        return typedArray.getInt(id, typedArray.getResources().getInteger(defaultId));
    }

    public static boolean getBoolean(
            TypedArray typedArray,
            int id,
            int defaultId) {
        return typedArray.getBoolean(id, typedArray.getResources().getBoolean(defaultId));
    }

    public static int getDimenSize(
            TypedArray typedArray,
            int id,
            int defaultId) {
        return typedArray.getDimensionPixelSize(id, typedArray.getResources().getDimensionPixelSize(defaultId));
    }

    public static int getDimenOffset(
            TypedArray typedArray,
            int id,
            int defaultId) {
        return typedArray.getDimensionPixelOffset(id, typedArray.getResources().getDimensionPixelOffset(defaultId));
    }

    public static int getColor(
            TypedArray typedArray,
            int id,
            int defaultId) {
        return typedArray.getColor(id, Util.getColor(typedArray, defaultId));
    }

    public static int getColor(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getResources().getColor(id, null);
        } else {
            //noinspection deprecation
            return context.getResources().getColor(id);
        }
    }

    public static int getColor(Context context, int id, int color) {
        if (id == 0) return color;
        else return getColor(context, id);
    }

    public static void setDrawable(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(drawable);
        } else {
            //noinspection deprecation
            view.setBackgroundDrawable(drawable);
        }
    }

    public static int getDarkerColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.9f;
        return Color.HSVToColor(hsv);
    }

    public static int getLighterColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 1.1f;
        return Color.HSVToColor(hsv);
    }

    public static int getColor() {
        Random random = new Random();
        while (true) {
            int colorIndex = random.nextInt(colors.length);
            if (!usedColor.contains(colorIndex)) {
                usedColor.add(colorIndex);
                while (usedColor.size() > 6) usedColor.remove(0);
                return colors[colorIndex];
            }
        }
    }

    public static boolean pointInView(PointF point, View view) {
        return view.getLeft() <= point.x && point.x <= view.getRight() &&
                view.getTop() <= point.y && point.y <= view.getBottom();
    }

    public static void setupUI(final Activity activityContext, View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Util.hideSoftKeyboard(activityContext);
                    return false;
                }
            });
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager)
                activity.getSystemService(Activity
                        .INPUT_METHOD_SERVICE);
        if (imm != null && activity.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(activity
                    .getCurrentFocus().getWindowToken(), 0);

    }

    public static boolean validateString(String userName) {
        return (userName.length() > 0 || !userName.equalsIgnoreCase(""));
    }

    public static boolean validateSpinner(final Spinner spinner, final String stringToCompare) {
        return (spinner.getSelectedItem().toString().equalsIgnoreCase(stringToCompare));
    }

    public static Util getInstance() {
        return ourInstance;
    }


    public static void saveUser(String userString, User userObj) {
    SharedPreferencesUtils.getInstance().writeStringValue(SharedPreferencesUtils.USER, userString);
    user = userObj;
    }

    public static User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public static TreeDetails getCurrentTree() {
        return currentTree;
    }

    public static void setCurrentTree(TreeDetails currentTree) {
        Util.currentTree = currentTree;
    }
}
