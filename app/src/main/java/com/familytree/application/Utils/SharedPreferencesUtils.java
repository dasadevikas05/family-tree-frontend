package com.familytree.application.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtils {
    public static final String ACCESS_TOKEN = "access_token";
    public static final String TOKEN_TYPE = "access_token";
    public static final String REFRESH_TOKEN = "access_token";
    public static final String MOBILE ="pref_mobile";
    public static final String EMAIL ="pref_email";

    public static final String USER ="user";


    private static final String HDFC_LIFE_PREFERENCES = "HDFC_LIFE_PREF"; // unique
    public static final String NAME = "pref_name";

    private static SharedPreferencesUtils sharedPreferencesUtils = null;
    private static SharedPreferences settings = null;

    private SharedPreferencesUtils() {


    }

    public static SharedPreferencesUtils getInstance() {
        if (sharedPreferencesUtils == null) {
            sharedPreferencesUtils = new SharedPreferencesUtils();
        }

        return sharedPreferencesUtils;
    }

    public void initializeSharePreferences(Context context) {
        settings = context.getSharedPreferences
                (SharedPreferencesUtils.HDFC_LIFE_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void writeStringValue(String key, String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String readStringValue(String key, String defaultValue) {
        return settings.getString(key, defaultValue);
    }

    public void writeLongValue(String key, Long value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public long readLongValue(String key, Long defaultValue) {
        return settings.getLong(key, defaultValue);
    }

    public boolean readBooleanValue(String key, Boolean defaultValue) {
        return settings.getBoolean(key, defaultValue);
    }

    public void writeBooleanValue(String key, Boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public int readIntegerValue(String key, Integer defaultValue) {
        return settings.getInt(key, defaultValue);
    }

    public void writeIntegerValue(String key, Integer value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void clearAllPref() {
        SharedPreferences.Editor editor = settings.edit();
        editor.clear().apply();
    }
}