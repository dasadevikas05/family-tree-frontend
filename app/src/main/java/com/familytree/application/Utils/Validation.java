package com.familytree.application.Utils;

import android.os.Bundle;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Validation {

    public static String checkString(String myString) {
        return ((myString == null) ||
                (myString.length() == 0) ||
                (myString.equalsIgnoreCase("null"))) ?
                "N/A" : myString.trim();
    }

    public boolean isEmailValid(String email) {
        String regExpn =
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        return matcher.matches();
    }

    public boolean isValidatedString(String string) {
        boolean isEmpty = true;
        if (string.equalsIgnoreCase("") || (string.equalsIgnoreCase("N/A"))) {
            isEmpty = false;
        } else {
            isEmpty = true;
        }
        return isEmpty;
    }

    public boolean isValidMobile(String phone2) {
        boolean check = false;
        CharSequence text = "";

        if (!Pattern.matches("[a-zA-Z]+", text)) {
            if (phone2.length() > 0 && !phone2.equalsIgnoreCase("") && (phone2.length() == 10)) {
                check = false;
                //  txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    public void validateStringAndSetTextView(TextView textView, String string) {
        if (string.equalsIgnoreCase("") || string.equalsIgnoreCase("null") || string.equalsIgnoreCase(null)) {
            textView.setText("N/A");
        } else {
            textView.setText(string);
        }
    }


    public void displayNormalSnackbar(CoordinatorLayout cdSnackbar, int intOfString) {
        Snackbar snackbar = Snackbar.make(cdSnackbar, intOfString, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public String isJsonObjectHasKeyContent(JSONObject jsonDataObject, String key) {
        String name = "";
        try {
            if (jsonDataObject.has(key) && !jsonDataObject.getString(key).equalsIgnoreCase("null")) {
                name = jsonDataObject.getString(key);
            } else {
                name = "N/A";
            }
        } catch (Exception e) {
            name = "N/A";
        }
        return name;
    }

    public String checkBundleParameter(Bundle bundle, String key) {
        String myKey = "";
        try {
            myKey = bundle.getString(key);
        } catch (Exception e) {
            e.printStackTrace();
            myKey = "";
        }
        return myKey;
    }


    public boolean validateSpinner(final Spinner spinner, final String stringToCompare) {
        if (spinner.getSelectedItem().toString().equalsIgnoreCase(stringToCompare)) {
            return true;
        } else {
            return false;
        }
    }


    public boolean validateContactNumber(String stringContact) {

        if (stringContact.length() < 10) {
            return true;
        } else {
            return false;
        }
    }

}
