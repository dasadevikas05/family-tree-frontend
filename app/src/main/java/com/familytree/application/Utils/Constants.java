package com.familytree.application.Utils;


public class Constants {

    public static final String MAIN_URL = "http://13.229.222.91:8080/";
    public static final String LOG_TAG = "FamilyTree ======";
    public static final String METHOD_REGISTER_USER ="registerUser" ;
    public static final String METHOD_FORGET_PASSWORD ="forgetPassword";
    public static final String METHOD_UPDATE_USER_DETAILS ="updateUser";
    public static final String METHOD_LOGIN ="oauth/token";
    public static final String METHOD_RESET_PASSWORD ="resetPassword" ;
    public static final String METHOD_VERIFY_OTP ="verifyOtp";
    public static final String METHOD_ADD_FEEDBACK ="addFeedback";
    public static final String METHOD_LOGOUT_USER ="logoutUser";
    public static final String METHOD_GET_TREE_LIST ="getMyTrees";
    public static final String METHOD_GET_TREE_DETAILS ="getTreeDetails";

    public static final String METHOD_CREATE_TREE ="createTree";
    public static final String METHOD_SEARCH_MEMBER ="serachMember";
    public static final String METHOD_ADD_MEMBER ="addMember";
    public static final String METHOD_REMOVE_MEMBER ="removeMember";




}
