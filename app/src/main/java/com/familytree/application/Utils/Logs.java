package com.familytree.application.Utils;

import android.util.Log;

import com.familytree.application.BuildConfig;


public class Logs {
    public static void D(String TAG, String MSG) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG + "==", MSG);
        }
    }

    public static void E(String TAG, String MSG) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG + "==", MSG);
        }
    }

    public static void V(String TAG, String MSG) {
        if (BuildConfig.DEBUG) {
            Log.v(TAG + "==", MSG);
        }
    }

    public static void I(String TAG, String MSG) {
        if (BuildConfig.DEBUG) {
            Log.i(TAG + "==", MSG);
        }
    }

    public static void W(String TAG, String MSG) {
        if (BuildConfig.DEBUG) {
            Log.w(TAG + "==", MSG);
        }
    }

}
