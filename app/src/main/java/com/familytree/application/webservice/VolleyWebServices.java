package com.familytree.application.webservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.familytree.application.Activity.OtpVerificationActivity;
import com.familytree.application.R;
import com.familytree.application.Utils.Constants;
import com.familytree.application.Utils.Util;
import com.familytree.application.interfaces.VolleyCallBack;
import com.familytree.application.interfaces.VolleyStringCallBack;
import com.google.android.material.snackbar.Snackbar;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.familytree.application.Utils.Util.isInternetAvailable;
import static com.familytree.application.webservice.VolleySingleton.getInstance;


public class VolleyWebServices {
    public static final int  INITIAL_TIME_OUT_MS = 50000;
    private VolleyStringCallBack stringListener = null;
    private Context mContext;
    private String url_validate = "&=*+-_.,:!?()/~%";
    private VolleySingleton requestQueue;
    private VolleyCallBack listner;
    ProgressDialog dialog;

    public VolleyWebServices(Context mContext) {
        this.mContext = mContext;
    }

    public void setStringListener(VolleyStringCallBack stringListner) {
        this.stringListener = stringListner;
    }

    public VolleyCallBack getListener() {
        return listner;
    }

    public void setListener(VolleyCallBack listner) {
        this.listner = listner;
    }

    public void myJsonPostRequest(final boolean showProgressDialog,
                                  final String methodUrl,
                                  final String methodName,
                                  final JSONObject postParams) {
        final View parentView = ((Activity) mContext).findViewById(android.R.id.content);
        if (isInternetAvailable(mContext)) {
            final ProgressDialog dialog = new ProgressDialog(mContext);
            dialog.setMessage("Loading..");
            dialog.setCancelable(false);
            if (showProgressDialog) {
                dialog.show();
            }
            Log.e(Constants.LOG_TAG, "PostRequest: " + methodUrl);
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);

            final JsonObjectRequest jsonObjReq  = new JsonObjectRequest(
                    Request.Method.POST,
                    methodUrl,
                    postParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject s) {
                            dialog.dismiss();
                            Log.e(Constants.LOG_TAG, "Response: " + s);
                            listner.getResponse(s, methodName);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dialog.dismiss();

                            if(error.networkResponse.statusCode == 208) {
                                try {
                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                    listner.getError(responseBody, "Error");
                                    Log.e("responseBody", "" + responseBody);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                showErrorDialog("Error", "Oops, Something went wrong try again later.");
                            }
                        }
                    }
            )

            {
                @Override
                public Map<String, String> getHeaders()throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }

            };
            jsonObjReq.setRetryPolicy(
                    new DefaultRetryPolicy(INITIAL_TIME_OUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjReq);
        } else {
            Snackbar snackbar = Snackbar.make(parentView, R.string.no_internet, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }




    public void myJsonPostStringRequest(final boolean showProgressDialog,
                                  final String methodUrl,
                                  final String methodName,
                                  final Map<String, String> postParams) {
        final View parentView = ((Activity) mContext).findViewById(android.R.id.content);
        if (isInternetAvailable(mContext)) {
            final ProgressDialog dialog = new ProgressDialog(mContext);
            dialog.setMessage("Loading..");
            dialog.setCancelable(false);
            if (showProgressDialog) {
                dialog.show();
            }
            Log.e(Constants.LOG_TAG, "PostRequest: " + methodUrl);
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);

            final StringRequest postRequest = new StringRequest(
                    Request.Method.POST,
                    methodUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            dialog.dismiss();
                            Log.e(Constants.LOG_TAG, "Response: " + s);
                            listner.getResponse(s, methodName);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            dialog.dismiss();
                            listner.getError(volleyError.getMessage(), methodName);

                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization",
                            "bearer  "+ Util.getUser().getAccessToken());
                    return params;
                }


            };
            postRequest.setRetryPolicy(
                    new DefaultRetryPolicy(INITIAL_TIME_OUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(postRequest);
        } else {
            Snackbar snackbar = Snackbar.make(parentView, R.string.no_internet, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }



    public void showErrorDialog(String title, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    public void sendFeedback(String feedback) {
        if (isInternetAvailable(mContext)) {
            final ProgressDialog dialog = new ProgressDialog(mContext);
            dialog.setMessage("Loading..");
            dialog.setCancelable(false);
            dialog.show();
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("feedback", feedback);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final JsonObjectRequest jsonObjReq  = new JsonObjectRequest(
                    Request.Method.POST,
                    Constants.MAIN_URL + Constants.METHOD_ADD_FEEDBACK,
                    jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject s) {
                            dialog.dismiss();
                            listner.getResponse(s, "");
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dialog.dismiss();

                            if(error.networkResponse.statusCode == 208) {
                                try {
                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                    listner.getError(responseBody, "Error");
                                    Log.e("responseBody", "" + responseBody);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                showErrorDialog("Error", "Oops, Something went wrong try again later.");
                            }
                        }
                    }
            )

            {
                @Override
                public Map<String, String> getHeaders()throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "bearer "+ Util.getUser().getAccessToken());
                    return params;
                }


            };
            jsonObjReq.setRetryPolicy(
                    new DefaultRetryPolicy(INITIAL_TIME_OUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjReq);
        } else {
            final View parentView = ((Activity) mContext).findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar.make(parentView, R.string.no_internet, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }



    private void showProgressDialog(String message) {
        if(dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
        dialog = new ProgressDialog(mContext);
        dialog.setMessage("Loading..");
        dialog.setCancelable(false);
        dialog.show();
    }


    private void hideProgressDialog() {
        if(dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }



    private void handleError(VolleyError error, String tag)  {

        hideProgressDialog();
        if(error.networkResponse.statusCode == 208) {
            try {
                String responseBody = new String(error.networkResponse.data, "utf-8");
                listner.getError(responseBody, tag);
                Log.e("responseBody", "" + responseBody);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            showErrorDialog("Error", "Oops, Something went wrong try again later.");
        }
    }


    private Map<String, String>  getHeaderObject() {

        Map<String, String>  params = new HashMap<String, String>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", "bearer "+ Util.getUser().getAccessToken());

        return  params;
    }


    private void handleNetworkError()  {
        final View parentView = ((Activity) mContext).findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(parentView, R.string.no_internet, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    public void post(JSONObject requestObject, String url,String tag) {
        if (isInternetAvailable(mContext)) {
            showProgressDialog("");
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            JsonObjectRequest jsonObjReq  = postRequest(requestObject,url, tag);
            jsonObjReq.setRetryPolicy(
                    new DefaultRetryPolicy(INITIAL_TIME_OUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjReq);
        } else {
            handleNetworkError();
        }
    }


    public void delete(JSONObject requestObject, String url,String tag) {
        if (isInternetAvailable(mContext)) {
            showProgressDialog("");
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            JsonObjectRequest jsonObjReq  = deleteRequest(requestObject,url, tag);
            jsonObjReq.setRetryPolicy(
                    new DefaultRetryPolicy(INITIAL_TIME_OUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjReq);
        } else {
            handleNetworkError();
        }
    }


    private JsonObjectRequest deleteRequest(JSONObject requestObject, String url, final String tag) {

        JsonObjectRequest jsonObjReq  = new JsonObjectRequest(
                Request.Method.DELETE,
                url,
                requestObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject s) {
                        hideProgressDialog();
                        listner.getResponse(s, tag);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleError(error, tag);
                    }
                }
        )

        {
            @Override
            public Map<String, String> getHeaders()throws AuthFailureError {
                return getHeaderObject();
            }
        };
        return  jsonObjReq;
    }



    private JsonObjectRequest postRequest(JSONObject requestObject, String url, final String tag) {

        JsonObjectRequest jsonObjReq  = new JsonObjectRequest(
                Request.Method.POST,
                url,
                requestObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject s) {
                        hideProgressDialog();
                        listner.getResponse(s, tag);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleError(error, tag);
                    }
                }
        )

        {
            @Override
            public Map<String, String> getHeaders()throws AuthFailureError {
                return getHeaderObject();
            }
        };
        return  jsonObjReq;
    }






    public void get(String url, String tag, boolean showProgress) {
        Log.e("GET URL : ", url);
        if (isInternetAvailable(mContext)) {
            if(showProgress) {
                showProgressDialog("");
            }
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest mStringRequest = getRequest(url, tag);
            mStringRequest.setRetryPolicy(
                    new DefaultRetryPolicy(INITIAL_TIME_OUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(mStringRequest);
        } else {
            handleNetworkError();
        }
    }


    private StringRequest getRequest(String url, final String tag) {

        StringRequest  mStringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                listner.getResponse(response, tag);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleError(error, tag);
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeaderObject();
            }
        };
        return  mStringRequest;
    }


    public void createTree(String treeName) {


        if (isInternetAvailable(mContext)) {
            final ProgressDialog dialog = new ProgressDialog(mContext);
            dialog.setMessage("Loading..");
            dialog.setCancelable(false);
            dialog.show();
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", treeName);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final JsonObjectRequest jsonObjReq  = new JsonObjectRequest(
                    Request.Method.POST,
                    Constants.MAIN_URL + Constants.METHOD_CREATE_TREE,
                    jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject s) {
                            dialog.dismiss();
                            listner.getResponse(s, "");
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dialog.dismiss();

                            if(error.networkResponse.statusCode == 208) {
                                try {
                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                    listner.getError(responseBody, "Error");
                                    Log.e("responseBody", "" + responseBody);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                showErrorDialog("Error", "Oops, Something went wrong try again later.");
                            }
                        }
                    }
            )

            {
                @Override
                public Map<String, String> getHeaders()throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "bearer "+ Util.getUser().getAccessToken());
                    Log.e("token===","bearer "+ Util.getUser().getAccessToken());
                    return params;
                }


            };
            jsonObjReq.setRetryPolicy(
                    new DefaultRetryPolicy(INITIAL_TIME_OUT_MS,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjReq);
        } else {
            final View parentView = ((Activity) mContext).findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar.make(parentView, R.string.no_internet, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}